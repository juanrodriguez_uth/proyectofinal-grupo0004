/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectofinal;
  import java.sql.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Statement;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.*;
import javax.swing.table.*;

public class Metodos {
    Conexion conec;
    
      public  void mostrarDatosCmb(String XX,JComboBox cmb) { 
    	String A[]=new String[1];
    	int i=0;
    	cmb.removeAllItems();
        try 
        { 
           conec = new Conexion();
            Statement s = conec.con.createStatement(); 
            ResultSet rs = s.executeQuery (XX); 
            // Recorremos el resultado, mientras haya registros para leer. 
            while (rs.next()) 
            { 
            	
            	cmb.addItem(""+rs.getObject(1));
                 i++;
        	
            } 
            // Cerramos la conexion a la base de datos. 
           conec.con.close(); 
        } 
        catch (Exception e) 
        { 
            e.printStackTrace(); 
        } 
     
    }
    
            public void llenarJtable(String sql, JTable tabla,String[] tit,int[] tamCamp)
        {
           try{
            conec = new Conexion();
            Statement s = conec.con.createStatement();
            ResultSet rs = s.executeQuery (sql);
            ResultSetMetaData rsm = rs.getMetaData();
            int cont = rsm.getColumnCount();
            DefaultTableModel modelo = new DefaultTableModel();
          for (int i = 0; i<tit.length; i++)
		{
			modelo.addColumn(tit[i]);
		}
		

		
            while(rs.next())
            {
                String fila[] = new String [cont];
                for(int j=0;j<cont;j++){
                    fila[j]=rs.getString(j+1);
                   
                } 
                modelo.addRow(fila);
            }
                tabla.setModel(modelo);
                
        for (int i = 0; i<tamCamp.length; i++)
		{
			TableColumn tablaid = null;
			tablaid = tabla.getColumnModel().getColumn(i);
			tablaid.setPreferredWidth(tamCamp[i]);
		}
		
		 tabla.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
 		 tabla.setColumnSelectionAllowed(true);
                rs.close();
                conec.con.close();
        }catch (Exception e) { 
            e.printStackTrace();}
        }
    //Metodo para limpiar todos los textFiedl
    public void limpiar_texto(JPanel panel,JTextField t){
        for(int i = 0; panel.getComponents().length > i; i++){
            if(panel.getComponents()[i] instanceof JTextField){
                ((JTextField)panel.getComponents()[i]).setText("");
            }
            else if(panel.getComponents()[i] instanceof JPasswordField){
                ((JPasswordField)panel.getComponents()[i]).setText("");
            }
        }
        t.requestFocus();
    }
    
     public void mostrarMensaje(String mensaje,int tipo){
         JOptionPane.showMessageDialog(null, mensaje,"",tipo);
     }
    
    	public void consulta_SQL(String XX){
       try 
         { 
           conec = new Conexion();
            Statement s = conec.con.createStatement(); 
             s.execute(XX);  
                     conec.con.close(); 
        } 
        catch (Exception e) 
        { 
        	mostrarMensaje("Quiza el contacto a guardar ya exista! U ocurrio un problema",0);
               
                return;
        	//JOptionPane.showMessageDialog(null,"Verificar la consulta \n"+XX,"ERROR",JOptionPane.ERROR_MESSAGE);
            //e.printStackTrace(); 
        } 

		
}
   void validarNumeros(JTextField txt)
    {
     txt.addKeyListener(new KeyAdapter(){
 public void keyTyped(KeyEvent e)
    {
          if(Character.isDigit(e.getKeyChar())||e.getKeyChar()=='.'||e.getKeyChar()==e.VK_ENTER||e.getKeyChar()=='+'||e.getKeyChar()=='-'||e.getKeyChar()==e.VK_SPACE)
            {}
            else
            {
                //JOptionPane.showMessageDialog(null, "Caracteres Permitidos\n[1,2,3,4,5,6,7,8,9,0,.]");
                 Object x=e.getSource();
                if(x.getClass().toString().equals("class javax.swing.JTextField"))
                {
                    JTextField temp=(JTextField) x;
                     temp.requestFocus();
                }
                if(x.getClass().toString().equals("class javax.swing.JFormattedTextField"))
                {
                    JFormattedTextField temp1=(JFormattedTextField) x;
                    temp1.requestFocus();
                }
                if(x.getClass().toString().equals("class javax.swing.JPasswordField"))
                {
                    JPasswordField temp2=(JPasswordField) x;
                  	temp2.requestFocus();
                }
                e.consume();
            }
    }
     });
   }
}
